# otmymodule module

Installation
  Silent install can be done with the switch /S (e.g. openturns-otmymodule-0.0-py27.exe /S).
  To test otmymodule module installation, in an OpenTURNS python console type:
    import otmymodule
    print otmymodule.__version__

Uninstallation
  OTMyModule module will be automatically uninstalled at OpenTURNS removal.
  OTMyModule module can be removed from OpenTURNS with uninst-template.exe uninstaller.
  Silent uninstall can be done with the switch /S.

