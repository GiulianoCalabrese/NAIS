#                                               -*- cmake -*-
#
#  OTMyModuleConfig.cmake(.in)
#
#  Find OTMyModule includes and libraries
#
#  Copyright 2005-2020 Airbus-EDF-IMACS-ONERA-Phimeca
#

# Use the following variables to compile and link against OTMyModule:
#  OTMYMODULE_FOUND          - True if OTMyModule was found on your system
#  OTMYMODULE_USE_FILE       - The file making OTMyModule usable
#  OTMYMODULE_DEFINITIONS    - Definitions needed to build with OTMyModule
#  OTMYMODULE_INCLUDE_DIRS   - List of directories where OTMyModule' header file are
#  OTMYMODULE_LIBRARY        - Library name
#  OTMYMODULE_LIBRARIES      - List of libraries to link against
#  OTMYMODULE_LIBRARY_DIRS   - List of directories containing OTMyModule' libraries
#  OTMYMODULE_ROOT_DIR       - The base directory of OTMyModule
#  OTMYMODULE_VERSION_STRING - A human-readable string containing the version
#  OTMYMODULE_VERSION_MAJOR  - The major version of OTMyModule
#  OTMYMODULE_VERSION_MINOR  - The minor version of OTMyModule
#  OTMYMODULE_VERSION_PATCH  - The patch version of OTMyModule

@PACKAGE_INIT@

set ( OTMYMODULE_FOUND 1 )
set ( OTMYMODULE_USE_FILE     "@PACKAGE_OTMYMODULE_USE_FILE@" )

set ( OTMYMODULE_DEFINITIONS  "@OTMYMODULE_DEFINITIONS@" )
set_and_check (OTMYMODULE_INCLUDE_DIR  "@PACKAGE_OTMYMODULE_INCLUDE_DIR@")
set ( OTMYMODULE_INCLUDE_DIRS "@PACKAGE_OTMYMODULE_INCLUDE_DIRS@" )
set_and_check (OTMYMODULE_LIBRARY      "@OTMYMODULE_LIBRARY@")
set ( OTMYMODULE_LIBRARIES    "@PACKAGE_OTMYMODULE_LIBRARIES@" )
set ( OTMYMODULE_LIBRARY_DIRS "@PACKAGE_OTMYMODULE_LIBRARY_DIRS@" )
set_and_check (OTMYMODULE_ROOT_DIR     "@PACKAGE_OTMYMODULE_ROOT_DIR@")

set ( OTMYMODULE_VERSION_STRING "@OTMYMODULE_VERSION_STRING@" )
set ( OTMYMODULE_VERSION_MAJOR  "@OTMYMODULE_VERSION_MAJOR@" )
set ( OTMYMODULE_VERSION_MINOR  "@OTMYMODULE_VERSION_MINOR@" )
set ( OTMYMODULE_VERSION_PATCH  "@OTMYMODULE_VERSION_PATCH@" )

set (OTMYMODULE_SWIG_INCLUDE_DIRS "@PACKAGE_OTMYMODULE_SWIG_INCLUDE_DIRS@")
set (OTMYMODULE_PYTHON@PYTHON_VERSION_MAJOR@_MODULE_PATH "@PACKAGE_PYTHON_MODULE_PATH@")

# Our library dependencies (contains definitions for IMPORTED targets)
include (${CMAKE_CURRENT_LIST_DIR}/OTMyModule-Targets.cmake)
