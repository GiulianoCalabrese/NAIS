#!/usr/bin/env python

from __future__ import print_function
import numpy as np
import openturns as ot
import NAIS as otmymodule

a = otmymodule.NAIS()
print(a)

#Definition of limit state function
"""
def four_branch(x):
    x1 = x[0]
    x2  = x[1]
    k = x[2]
    
    g1 = 3+0.1*(x1-x2)**2-(x1+x2)/np.sqrt(2)
    g2 = 3+0.1*(x1-x2)**2+(x1+x2)/np.sqrt(2)
    g3 = (x1-x2)+k/np.sqrt(2)
    g4 =(x2-x1)+k/np.sqrt(2)
    
    return [min((g1,g2,g3,g4))]
"""
formulas = ["def four_branch(x):"]
formulas += ["g1 = 3+0.1*(x1-x2)**2-(x1+x2)/np.sqrt(2);"]
formulas += ["g2 = 3+0.1*(x1-x2)**2+(x1+x2)/np.sqrt(2);"]
formulas += ["g3 = (x1-x2)+k/np.sqrt(2);"]
formulas += ["g4 =(x2-x1)+k/np.sqrt(2);"]
formulas += ["return [min((g1,g2,g3,g4))]"]


# Definition de la pythonfunction generale
#my_four_branch = ot.PythonFunction(3, 1, four_branch)
my_four_branch = ot.PythonFunction(3, 1, four_branch)

# Transformation de la pythonfunction vers parametricfunction en figeant le parametre k 
index_frozen = [2]
my_four_branch_6 = ot.ParametricFunction(my_four_branch, index_frozen, [6])
my_four_branch_7 = ot.ParametricFunction(my_four_branch, index_frozen, [7])

#Definition of input variable PDF

dim_inputs = 2
dist_x = ot.Normal([0.0, 0.0], [1.0, 1.0], ot.CorrelationMatrix(dim_inputs))
inputVector = ot.RandomVector(dist_x)

#Determination of reference probability
#MonteCarlo experiment

n_MC = np.int(1e6)

# Creation of event
ot.RandomGenerator.SetSeed(1)

vect = ot.RandomVector(dist_x)
G = ot.CompositeRandomVector(my_four_branch_7, vect)
event = ot.ThresholdEvent(G, ot.Less(), 0.0)

# Hyperparameters of the algorithm
n_IS= 10 # Number of samples at each iteration
rho_quantile= 10 # Quantile determining the percentage of failure samples in the current population 

# Definition of the algoritm
NAIS = otmymodule.NAISImplementation(event,n_IS,rho_quantile)

# Run of the algorithm
NAIS.run()
NAIS_result = NAIS.getResult()

print('Probability of failure:',NAIS_result.getProbabilityEstimate())
print('Samples:',NAIS_result.getSamples())
