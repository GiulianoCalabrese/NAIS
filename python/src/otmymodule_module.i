// SWIG file otmymodule_module.i

%module(docstring="otmymodule module") otmymodule

%{
#include <openturns/OT.hxx>
#include <openturns/PythonWrappingFunctions.hxx>
%}

// Prerequisites needed
%include typemaps.i
%include exception.i
%ignore *::load(OT::Advocate & adv);
%ignore *::save(OT::Advocate & adv) const;

%import base_module.i
%import uncertainty_module.i

// The new classes
%include otmymodule/OTMyModuleprivate.hxx
%include NAISImplementation.i
%include NAIS.i


