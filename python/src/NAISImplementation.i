// SWIG file NAISImplementation.i

%{
#include "otmymodule/NAISImplementation.hxx"
%}

%include NAISImplementation_doc.i

%template(NAISImplementationdInterfaceObject)           OT::TypedInterfaceObject<OTMYMODULE::NAISImplementation>;

%include otmymodule/NAISImplementation.hxx
namespace OTMYMODULE { %extend NAISImplementation { NAISImplementation(const NAISImplementation & other) { return new OTMYMODULE::NAISImplementation(other); } } }
