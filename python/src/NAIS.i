// SWIG file NAIS.i

%{
#include "otmymodule/NAIS.hxx"
%}

%include NAIS_doc.i

TypedInterfaceObjectImplementationHelper(OTMYMODULE, NAIS, NAISImplementation)

%include otmymodule/NAIS.hxx
namespace OTMYMODULE { %extend NAIS { NAIS(const NAIS & other) { return new OTMYMODULE::NAIS(other); } } }
