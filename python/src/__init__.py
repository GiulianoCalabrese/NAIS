"""
    otmymodule --- An OpenTURNS module
    ==================================

    Contents
    --------
      'otmymodule' is a module for OpenTURNS

"""

import sys
if sys.platform.startswith('win'):
    # this ensures OT dll is loaded
    import openturns

from .otmymodule import *

__version__ = '0.0'

