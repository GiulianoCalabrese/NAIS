
#ifndef OTMYMODULE_PRIVATE_HXX
#define OTMYMODULE_PRIVATE_HXX

/* From http://gcc.gnu.org/wiki/Visibility */
/* Generic helper definitions for shared library support */
#if defined _WIN32 || defined __CYGWIN__
#define OTMYMODULE_HELPER_DLL_IMPORT __declspec(dllimport)
#define OTMYMODULE_HELPER_DLL_EXPORT __declspec(dllexport)
#define OTMYMODULE_HELPER_DLL_LOCAL
#else
#if __GNUC__ >= 4
#define OTMYMODULE_HELPER_DLL_IMPORT __attribute__ ((visibility ("default")))
#define OTMYMODULE_HELPER_DLL_EXPORT __attribute__ ((visibility ("default")))
#define OTMYMODULE_HELPER_DLL_LOCAL  __attribute__ ((visibility ("hidden")))
#else
#define OTMYMODULE_HELPER_DLL_IMPORT
#define OTMYMODULE_HELPER_DLL_EXPORT
#define OTMYMODULE_HELPER_DLL_LOCAL
#endif
#endif

/* Now we use the generic helper definitions above to define OTMYMODULE_API and OTMYMODULE_LOCAL.
 * OTMYMODULE_API is used for the public API symbols. It either DLL imports or DLL exports (or does nothing for static build)
 * OTMYMODULE_LOCAL is used for non-api symbols. */

#ifndef OTMYMODULE_STATIC /* defined if OT is compiled as a DLL */
#ifdef OTMYMODULE_DLL_EXPORTS /* defined if we are building the OT DLL (instead of using it) */
#define OTMYMODULE_API OTMYMODULE_HELPER_DLL_EXPORT
#else
#define OTMYMODULE_API OTMYMODULE_HELPER_DLL_IMPORT
#endif /* OTMYMODULE_DLL_EXPORTS */
#define OTMYMODULE_LOCAL OTMYMODULE_HELPER_DLL_LOCAL
#else /* OTMYMODULE_STATIC is defined: this means OT is a static lib. */
#define OTMYMODULE_API
#define OTMYMODULE_LOCAL
#endif /* !OTMYMODULE_STATIC */


#endif // OTMYMODULE_PRIVATE_HXX

