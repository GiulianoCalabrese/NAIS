#include <iostream>
#include <algorithm>
#include <string>

#include "otmymodule/NAISImplementation.hxx"
#include "otmymodule/NAIS.hxx"
#include "openturns/ParametricFunction.hxx"
#include "openturns/RandomGenerator.hxx"
#include "openturns/CompositeRandomVector.hxx"
#include "openturns/Less.hxx"
#include "openturns/SymbolicFunction.hxx"
#include "otmymodule/OTMyModule.hxx"

using namespace OT;
using namespace OTMYMODULE;

int main()
{
	NAIS a;
	a.setProbabilityEstimate(0.67);
	//std::cout << a.getProbabilityEstimate() << std::endl;


/*	String formula; 
	formula += "float g1 = 3+0.1*std::pow(x1-x2,2)-(x1+x2)/std::sqrt(2)";
	formula += "float g2 = 3+0.1*std::pow(x1-x2,2)+(x1+x2)/std::sqrt(2)";
	formula += "float g3 = (x1-x2)+x3/std::sqrt(2)";
	formula += "float g4 =(x2-x1)+x3/std::sqrt(2)";
	formula += "return std::min({g1,g2,g3,g4})";
	
	// Definition de la pythonfunction generale
	std::string string_variable = "x1";
	string_variable.append("x2");
	string_variable.append("x3");
	String input_variables = string_variable;
	Function my_four_branch = SymbolicFunction(input_variables, formula);
	
	
	// Transformation de la pythonfunction vers parametricfunction en figeant le parametre k 
	Indices index_frozen = Indices(2);
	Point identif_point6(6);
	Point identif_point7(7);
//	ParametricFunction my_four_branch_7(my_four_branch, index_frozen, identif_point7);

	// Definition of input variable PDF
	int dim_inputs = 2;
	Point mean(2,(0.0, 0.0)) ;
	Point sigma(2,(1.0, 1.0)) ;
	Normal dist_x(mean, sigma, CorrelationMatrix(dim_inputs));
	RandomVector inputVector = RandomVector(dist_x);

	// Determination of reference probability
	// MonteCarlo experiment
	int n_MC = 1000000;

	// Creation of event
	RandomGenerator::SetSeed(1);

	RandomVector vect = RandomVector(dist_x);
/*	CompositeRandomVector G = CompositeRandomVector(my_four_branch, vect);
	ThresholdEvent event = ThresholdEvent(G, Less(), 0.0);

	// Hyperparameters of the algorithm
	// Number of samples at each iteration
	int n_IS= 10 ;
	// Quantile determining the percentage of failure samples in the current population 
	float rho_quantile = 10 ;

	// Definition of the algoritm
	NAISResultimplementation NAIS = NAISResultimplementation(event,n_IS,rho_quantile);

	// Run of the algorithm
	NAIS.run();
	NAISResult NAIS_result = NAIS.getResult();

	std::cout << "Probability of failure:" << NAIS_result.getProbabilityEstimate()<< std::endl;
	std::cout << "Samples:" << NAIS_result.getSamples()<< std::endl;
*/
	return 0;
}

