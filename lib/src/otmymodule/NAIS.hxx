//                                               -*- C++ -*-
/**
 *  @brief NAIS
 *
 *  Copyright 2005-2020 Airbus-EDF-IMACS-ONERA-Phimeca
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef OTMYMODULE_NAIS_HXX
#define OTMYMODULE_NAIS_HXX

#include <openturns/TypedInterfaceObject.hxx>
#include <openturns/StorageManager.hxx>
#include <openturns/Point.hxx>
#include <openturns/ComposedDistribution.hxx>
#include <openturns/Sample.hxx>
#include "otmymodule/OTMyModuleprivate.hxx"

namespace OTMYMODULE
{

/* forward declaration */
class NAISImplementation;

/**
 * @class NAIS
 *
 * NAIS is some nais type to illustrate how to add some classes in Open TURNS
 */
class OTMYMODULE_API NAIS
  : public OT::TypedInterfaceObject<NAISImplementation>
{
  CLASSNAME;

public:

  /** Default constructor */
  NAIS();
  
  /** Constructor from implementation */
  NAIS(const NAISImplementation & implementation);

/** get Probability Estimate */	        
    float getProbabilityEstimate();

/** set Probability Estimate */	    
    void setProbabilityEstimate(float proba);

/** get Samples */	    
    OT::Sample getSamples();

/** set NAIS Samples */	    
    void setNAISSamples(OT::Sample samples);

/** get Auxiliary Density */	        
    OT::ComposedDistribution getAuxiliaryDensity();

/** set Auxiliary Density */	    
    void setAuxiliaryDensity(OT::ComposedDistribution density);
        
// The fixed sample that will be returned at each call
    float ProbabilityEstimate;
    OT::Sample Samples;
    OT::ComposedDistribution aux_distrib;

private:

}; /* class NAIS */

} /* namespace OTMYMODULE */

#endif /* OTMYMODULE_NAIS_HXX */
