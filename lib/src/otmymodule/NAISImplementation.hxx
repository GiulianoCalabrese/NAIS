//                                               -*- C++ -*-
/**
 *  @brief NAISImplementation
 *
 *  Copyright 2005-2020 Airbus-EDF-IMACS-ONERA-Phimeca
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef OTMYMODULE_NAISIMPLEMENTATION_HXX
#define OTMYMODULE_NAISIMPLEMENTATION_HXX

#include <openturns/PersistentObject.hxx>
#include <openturns/StorageManager.hxx>
#include <openturns/Point.hxx>
#include "otmymodule/OTMyModuleprivate.hxx"
#include <openturns/ThresholdEvent.hxx>
#include <openturns/PersistentObjectFactory.hxx>
#include <openturns/Mixture.hxx>
#include <openturns/ComposedDistribution.hxx>
#include <openturns/Normal.hxx>
#include "otmymodule/NAIS.hxx"

namespace OTMYMODULE
{

/**
 * @class NAISImplementation
 *
 * NAISImplementation is some nais type to illustrate how to add some classes in OpenTURNS
 */
class OTMYMODULE_API NAISImplementation
  : public OT::PersistentObject
{
  CLASSNAME;

public:

  /** Default constructor */
  NAISImplementation();

  /** Default constructor */
    NAISImplementation(OT::ThresholdEvent event,int n_IS_value, float rho_quantile);

  /** Virtual constructor method */
  NAISImplementation * clone() const override;

/** get weights */	        
    OT::Sample getWeights();

/** set weights */	    
    void setWeights(OT::Sample weigth_);

/** get outputsamples */	        
    OT::Sample getOutputsamples();

/** set outputsamples */	    
    void setOutputsamples(OT::Sample outputsamples_);
	
/** Function computing the auxiliary distribution as a function of current samples and associated weights */	        
    OT::ComposedDistribution compute_aux_distribution(OT::Sample sample,OT::Sample weights); 

/** Function defining if samples is in failure mode */	    
    std::vector<bool> is_failure(OT::Sample Y, float S_loc);

/** Function computing weigths  of sample */    
    OT::Sample compute_weights(OT::Sample samples, OT::Sample resp_samples, float S_loc, OT::Distribution aux_distrib);

/** Main function that computes the failure probability */ 	    
    void run();

/** Accessor to results */        
    NAIS getResult();

  
// The fixed sample that will be returned at each call
  int n_IS;
  int nb_eval; // Current number of evaluations
  OT::Sample weights; // Current weights samples 
  float S;
  OT::Sample sample;
  OT::UnsignedInteger dim;
  OT::Sample outputsamples; // Current output samples
  OT::Distribution distrib;
  float rho_quantile;
  NAIS result;
  OT::ComparisonOperator operator_; // event operator
  OT::Function limit_state_function;
  float probabilityEstimate;

private:

}; /* class NAISImplementation */

} /* namespace OTMYMODULE */

#endif /* OTMYMODULE_NAISIMPLEMENTATION_HXX */
