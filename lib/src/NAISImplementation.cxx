//                                               -*- C++ -*-
/**
 *  @brief NAISImplementation
 *
 *  Copyright 2005-2020 Airbus-EDF-IMACS-ONERA-Phimeca
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "otmymodule/NAISImplementation.hxx"
#include <openturns/PersistentObjectFactory.hxx>

using namespace OT;

namespace OTMYMODULE
{

CLASSNAMEINIT(NAISImplementation);

static Factory<NAISImplementation> Factory_NAISImplementation;

/* Default constructor */
NAISImplementation::NAISImplementation()
  : PersistentObject()
{
  // Nothing to do
}

/* Default constructor */
NAISImplementation::NAISImplementation(ThresholdEvent event,int n_IS_value, float rho_quantile)
  : PersistentObject()
	{
		// Number of samples at each iteration
		n_IS = n_IS_value;
		// Limit state function
		Function limit_state_function = event.getFunction(); 
		// Failure threshold
		S = event.getThreshold(); 
		// Dimension of input space
		UnsignedInteger dim = event.getAntecedent().getDimension();
		float proba = 0;
		// Initial distribution
		Distribution distrib = event.getAntecedent().getDistribution(); 
		// Verification of unbounded distribution
		Interval range_ = distrib.getRange();
		bool UpperBound = false ; bool LowerBound = false;
		Interval::BoolCollection rangeUpper = range_.getFiniteUpperBound();
		Interval::BoolCollection rangeLower = range_.getFiniteLowerBound();
		for (int i=0; i < rangeUpper.getSize(); i++)
		{
		    if (rangeUpper[i] > 0)
		    {
		        UpperBound = true;
		    }
		}    
		for (int i=0; i < rangeLower.getSize(); i++)
		{
		    if (rangeLower[i] > 0)
		    {
		        LowerBound = true;
		    }
		}		
		if (UpperBound == true || LowerBound == true) 
		{
		    throw std::invalid_argument("Current version of NAIS is only adapted to unbounded distribution");
		}		
		ComparisonOperator failure_condition =  event.getOperator();
		if (failure_condition(0,1) == true)
		{
		    NAISImplementation::rho_quantile = rho_quantile/100; // definition of rho quantile if exceedance probability
		}
		else
		{
		    NAISImplementation::rho_quantile = 1- rho_quantile/100; // definition of rho quantile in case g<0
		}
		nb_eval = 0; 
		weights = Sample();
		outputsamples = Sample();
		operator_ = failure_condition; 
		result = NAIS();
	}

/* Virtual constructor method */
NAISImplementation * NAISImplementation::clone() const
{
  return new NAISImplementation(*this);
}

/** get weights */	        
Sample NAISImplementation::getWeights()
    {
    	return weights;
    }

/** set weights */	    
void NAISImplementation::setWeights(Sample weight_)
    {
    	weights = weight_;
    }


/** get outputsamples */	        
Sample NAISImplementation::getOutputsamples()
    {
    	return outputsamples;
    }

/** set outputsamples */	    
void NAISImplementation::setOutputsamples(Sample outputsamples_)
    {
    	outputsamples = outputsamples_;
    }

/** Function computing the auxiliary distribution as a function of current samples and associated weights */
	ComposedDistribution NAISImplementation::compute_aux_distribution(Sample sample, Sample weights)
	{
	    	float weights_pow2=0;
	    	Point weights_add;
	    	for (int i = 0; i< weights.getSize(); i++)
	    	{
	    		Point weights_point = weights[i];
	    		weights_pow2+=weights_point.dot(weights[i]);
	    		weights_add+=weights[i];
    		}
		float neff = std::pow(weights_add[0],2) / weights_pow2; // computation of weight
			
		// computation of bandwidth using Silverman rule
		Point stdPerComponent = sample.computeStandardDeviationPerComponent();
		std::vector<float> silverman;
		for (int i = 0; stdPerComponent.getSize(); i++)
	    	{
	    		silverman.push_back(stdPerComponent[i]*std::pow(neff * (dim + 2) / 4.,-1. / (dim + 4)));
    		}
		
		Collection<Distribution> margins;
			
		// computation of auxiliary distribution using ot.Mixture 
		for (int k = 0; k < dim ; k++) 
		{
		    Collection<Distribution> dist_coll;
		    for (int i= 0; i < n_IS ; i++)
		    {
			dist_coll.add(Normal(sample[i][k],silverman[k]));
		    }
		    Mixture distri_margin = Mixture(dist_coll,weights.asPoint());
		    margins.add(distri_margin);      
		
		result.aux_distrib = ComposedDistribution(margins);  
		return result.aux_distrib;
		}
	}    
	
/** Function defining if samples is in failure mode */
	std::vector<bool> NAISImplementation::is_failure(Sample Y, float S_loc)
	{
		//Sample weights_bool(Y.getSize(),1);
		std::vector<bool> weights_bool;
		Point S_loc_point = Point(1,S_loc);
		Point zero = Point(1,0.0);
		for(int i =0; i < Y.getSize() ; i++)
		{
			if (operator_(0,1) == true)
			{
				weights_bool.push_back(Y[i]<S_loc_point);
			}
			else
			{
				weights_bool.push_back(S_loc_point-Y[i]<zero);
			}
		}
		return weights_bool;
	}     

/** Function computing weigths  of sample */
Sample NAISImplementation::compute_weights(Sample samples,Sample resp_samples,float S_loc,Distribution aux_distrib)
	{        
		Sample f_value = distrib.computePDF(samples);
		Point g_value = result.aux_distrib.computePDF(samples).asPoint();
		Sample fraction;
		for (int i =0; i< f_value.getSize(); i++)
		{
			fraction.add(f_value[i]/g_value[i]);
		}
		
		std::vector<bool> weights_bool = is_failure(resp_samples,S_loc);
		for (int i =0; i< fraction.getSize(); i++)
		{
			weights.add( weights_bool[i]*fraction[i]);
		}
		
		return weights;
	}
	
/** Main function that computes the failure probability */    
void NAISImplementation::run()
	{      
	sample = distrib.getSample(n_IS); // drawing of samples using initial density
	Sample resp_sample = limit_state_function(sample) ;// evaluation on limit state function
	float quantile_courant = resp_sample.computeQuantile(rho_quantile).getCollection()[0]; // computation of current quantile
	// computation of weights
	weights = NAISImplementation::compute_weights(sample,resp_sample,quantile_courant,distrib) ;
	ComposedDistribution aux_distrib = NAISImplementation::compute_aux_distribution(sample,weights); // computation of auxiliary distribution

	while (operator_(S,quantile_courant)==true)
	{        
		sample = aux_distrib.getSample(n_IS); // drawing of samples using auxiliary density
		resp_sample = limit_state_function(sample); // evaluation on limit state function
		quantile_courant = resp_sample.computeQuantile(rho_quantile)[0]; // computation of current quantile

		if (operator_(quantile_courant,S))
		{
		quantile_courant = S;
		}
		else
		{
		weights = compute_weights(sample,resp_sample,quantile_courant,aux_distrib); // computation of weights
		aux_distrib = compute_aux_distribution(sample,weights); //update of auxiliary distribution;
		}
	}
	std::vector<bool> y;
	// Estimation of failure probability
	for (int i=0;i < resp_sample.getSize(); i++) 
	{
		y.push_back(operator_(resp_sample(i,0),S)); // find failure points
	}
	Collection<UnsignedInteger> indices_critic;// find failure samples indices
	for(int i=0;i<y.size();i++)
	{
	    if (y[i] = true)
	    {
		indices_critic.add(i);
	    }
	}
	Sample resp_sample_critic = resp_sample.select(indices_critic);
	Sample sample_critic = sample.select(indices_critic);

	Sample pdf_init_critic = distrib.computePDF(sample_critic); //evaluate initial PDF on failure samples
	Point pdf_aux_critic = aux_distrib.computePDF(sample_critic).asPoint(); //evaluate auxiliary PDF on failure samples
	
	float sum_pdf_critic=0;
	for(int i=0;i<pdf_init_critic.getSize();i++)
	{
	    sum_pdf_critic+= (pdf_init_critic[i]/pdf_aux_critic[i]).getCollection()[0];
	}
	probabilityEstimate = (1/n_IS) * sum_pdf_critic ;//Calculation of failure probability
	//aux_distrib = aux_distrib;

	// Save of data in SimulationResult structure
	result.setProbabilityEstimate(probabilityEstimate);
	result.setNAISSamples(sample);
	result.setAuxiliaryDensity(aux_distrib);
	outputsamples = resp_sample;
	}
	
/** Accessor to results */
	OTMYMODULE::NAIS NAISImplementation::getResult()
	{
		return result;
	}	

} /* namespace OTMYMODULE */
