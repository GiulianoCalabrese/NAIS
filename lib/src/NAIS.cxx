//                                               -*- C++ -*-
/**
 *  @brief NAIS
 *
 *  Copyright 2005-2020 Airbus-EDF-IMACS-ONERA-Phimeca
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "otmymodule/NAIS.hxx"
#include "otmymodule/NAISImplementation.hxx"
#include <openturns/PersistentObjectFactory.hxx>

using namespace OT;

namespace OTMYMODULE
{

CLASSNAMEINIT(NAIS);

/* Default constructor */
NAIS::NAIS()
  : TypedInterfaceObject<NAISImplementation>(new NAISImplementation)
{
  // Nothing to do
}

NAIS::NAIS(const NAISImplementation & implementation)
  : TypedInterfaceObject<NAISImplementation>(implementation.clone())
{
  // Nothing to do
}

/** get Probability Estimate */	        
float NAIS::getProbabilityEstimate()
    {
    	return ProbabilityEstimate;
    }

/** set Probability Estimate */	    
void NAIS::setProbabilityEstimate(float proba)
    {
    	ProbabilityEstimate = proba;
    }

/** get Samples */	    
Sample NAIS::getSamples()
    {
    	return Samples;
    }
/** set NAIS Samples */	    
void NAIS::setNAISSamples(Sample samples)
    {
    	Samples = samples;
    }

/** get Auxiliary Density */	        
ComposedDistribution NAIS::getAuxiliaryDensity()
    {
    	return aux_distrib;
    }
/** set Auxiliary Density */	    
void NAIS::setAuxiliaryDensity(ComposedDistribution density)
    {
    	aux_distrib = density;
    }


} /* namespace OTMYMODULE */
